provider "aws" { 
    region = "us-west-1"
}

terraform{
     backend "s3"{
          bucket = "ea-terraform-training-lab"
          key = "josecantor/terraform.tfstate"
          region = "us-west-1"
     }
}